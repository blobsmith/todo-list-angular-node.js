module.exports = {
    port: 3000,
    publicDirectory: 'public',
    viewsDirectory: '/views',
    viewEngine: 'mustache',
    logger: 'dev',
    appConf: 'development',
    dbHost: 'localhost',
    dbName: 'appli'
};
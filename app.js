var express = require('express')
  , http = require('http')
  , path = require('path')
  , config = require('./config')
  , router = require('./router')
  , mongoose = require('mongoose');

var app = express();

mongoose.connect(config.dbHost, config.dbName);

app.configure(function(){
    app.set('port', process.env.PORT || config.port);
    app.set('views', __dirname + config.viewsDirectory);
    app.set('view engine', config.viewEngine);
    app.engine(config.viewEngine, require('hogan-middleware').__express);
    app.use(express.favicon());
    app.use(express.logger(config.logger));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, config.publicDirectory)));
});

app.configure(config.appConf, function(){
    app.use(express.errorHandler());
});

router.getRoutes(app);

http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});

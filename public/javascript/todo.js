angular.module('todoApp', [])
    .controller('TodoController', ['$scope', '$http', function($scope, $http) {

        $http.get('/service/v1/todos').
            success(function(data, status, headers, config) {
                $scope.todos = data;
            }).
            error(function(data, status, headers, config) {
                console.log('Get data error from rest service');
            });

        $scope.addTodo = function() {
            $http.post('/service/v1/todos', {title: $scope.todoText}).
                success(function(data, status, headers, config) {
                    if(data.status === 'ok'){
                        $scope.todos.push(data.todo);
                    }
                }).
                error(function(data, status, headers, config) {
                    console.log('Post error from rest service');
                });
            $scope.todoText = '';
        };

        $scope.removeTodo = function(id){
            $http.delete('/service/v1/todos/'+id, {}).
                success(function(data, status, headers, config) {
                    angular.forEach($scope.todos, function(todo, key) {
                        if(todo._id === id){
                            $scope.todos.splice(key, 1);
                        }
                    });
                }).
                error(function(data, status, headers, config) {
                    console.log('Delete error from rest service');
                });
        };

        $scope.doneToto = function(id, done){
            $http.patch('/service/v1/todos/'+id, {done: done}).
                success(function(data, status, headers, config) {
                }).
                error(function(data, status, headers, config) {
                    console.log('Patch error from rest service');
                });
        }

        $scope.remaining = function() {
            var count = 0;
            angular.forEach($scope.todos, function(todo) {
                count += todo.done ? 0 : 1;
            });
            return count;
        };

        $scope.archive = function() {
            var oldTodos = $scope.todos;
            $scope.todos = [];
            angular.forEach(oldTodos, function(todo) {
                if (!todo.done) $scope.todos.push(todo);
            });
        };
    }]);
var mongooseModule = require('mongoose')
    , flow = require('../flow');

(function(mongoose){

    /**
     * Définition de la structure de todo
     */
    var schema = mongoose.Schema({
        date: 'date',
        title: 'string',
        done: Boolean
    });

    /**
     * Création d'un todo
     */
    schema.statics.addTodo = function (title, callback) {
        var NewTodo = new Todo({
            date: new Date(),
            title: title,
            done: false
        });
        NewTodo.save(callback);
    };

    /**
     * Suppression d'un todo
     */
    schema.statics.removeTodo = function (id, callback) {
        Todo.findOneAndRemove({_id: id}, callback);
    };

    /**
     * Modification d'un todo
     */
    schema.statics.modifyTodo = function (id, data, callback) {
        Todo.findByIdAndUpdate(id, data, callback);
    };

    /**
     * Création de données de test en db
     */
    schema.statics.loadData = function (callback) {
        for(var i=0;i<10;i++){
            this.addTodo('Todo Test title '+i, callback);
        }
    };

    /**
     * Initialisation des données de la db
     */
    schema.statics.initialize = function (callback) {
        var self = this;
        flow.exec(

            //Suppression des données
            function(){
                Todo.find({}).remove(this);
            },

            //Création des données de test
            function(err){
                if(!err){
                    if(self){
                        self.loadData.call(self,this.MULTI);
                    }
                    else{
                        this();
                    }
                }
            },

            //Renvoi des données
            function(data){
                var todo = new Array();
                for(key in data){
                    todo.push(data[key][1]);
                }
                callBack(todo);
            }
        );
    };

    var Todo = mongoose.model('Todo', schema);
    exports.model = Todo;


})(mongooseModule)
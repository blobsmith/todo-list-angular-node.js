var Todo = require('../models/TodoModel');

(function(){
   var actions = {

       /**
        * Recuperation de toutes les todos
        * @param req
        * @param res
        */
        todo: function(req, res){
           Todo.model.find({}, function(error, data){
               res.send(data);
           });
        },

       /**
        * Ajout d'une item dans la liste
        * @param req
        * @param res
        */
       addTodo: function(req, res){
           Todo.model.addTodo(req.body.title, function(error, data){
               res.send({status: 'ok', todo: data});
           });
       },

       /**
        * Supprime une item dans la liste
        * @param req
        * @param res
        */
       removeTodo: function(req, res){
           Todo.model.removeTodo(req.params.id, function(error, data){
               res.send({status: 'ok'});
           });
       },

       /**
        * Modifie une item de la liste
        * @param req
        * @param res
        */
       modifyTodo: function(req, res){
           if(req.body.done !== undefined ){
               Todo.model.modifyTodo(req.params.id, {done: req.body.done}, function(error, data){
                   res.send({status: 'ok', todo: data});
               });
           }
           else{
               res.send({status: 'ko'});
           }
       },

       /**
        * A utiliser pour populer MongoDB
        * @param req
        * @param res
        */
       initDB: function(req, res){
           Todo.model.initialize(function(data){
               res.send(data);
           });
       }
   };

    module.exports = actions;
})()
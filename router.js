var mainController = require('./controllers/mainController');

(function(controller){
    var routing = {
        getRoutes: function(app){
            app.get('/service/v1/todos', controller.todo);
            app.post('/service/v1/todos', controller.addTodo);
            app.delete('/service/v1/todos/:id', controller.removeTodo);
            app.patch('/service/v1/todos/:id', controller.modifyTodo);
            app.get('/service/v1/init', controller.initDB);
        }
    };
    exports.getRoutes = routing.getRoutes;
})(mainController)